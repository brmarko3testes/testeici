﻿using System.Web.Mvc;

namespace ProvaCandidato.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.NomeEmpresa = System.Configuration.ConfigurationManager.AppSettings["nomeempresa"];
			return View();
		}
	}
}