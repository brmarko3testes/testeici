﻿using ProvaCandidato.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ProvaCandidato.Controllers.Common
{
	public abstract class BasicCRUDController<TEntidade> : Controller where TEntidade : class
	{
		protected ContextoPrincipal db = new ContextoPrincipal();

		public ActionResult Index() => View(db.Set<TEntidade>().ToList());

		[HttpGet]
		public ActionResult Details(int? id)
		{
			if (id == null)
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			var entidade = db.Set<TEntidade>().Find(id); // Make async.

			return entidade == null ?
				HttpNotFound() :
				(ActionResult) View(entidade);
		}

		[HttpGet]
		public ActionResult Create() => View();

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(TEntidade entidade)
		{
			if (ModelState.IsValid)
			{
				db.Set<TEntidade>().Add(entidade);
				db.SaveChanges(); // Make async.
				return RedirectToAction("Index");
			}

			return View(entidade);
		}

		public ActionResult Edit(int? id)
		{
			if (id == null)
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			var entidade = db.Set<TEntidade>().Find(id);

			return entidade == null ?
				HttpNotFound() :
				(ActionResult) View(entidade);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(TEntidade entidade)
		{
			if (ModelState.IsValid)
			{
				db.Entry(entidade).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(entidade);
		}

		public ActionResult Delete(int? id)
		{
			if (id == null)
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			var entidade = db.Set<TEntidade>().Find(id); // Make async.

			return entidade == null ?
				HttpNotFound() :
				(ActionResult) View(entidade);
		}

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			var entidade = db.Set<TEntidade>().Find(id);
			db.Set<TEntidade>().Remove(entidade);
			db.SaveChanges(); // Make async.
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
				db.Dispose();

			base.Dispose(disposing);
		}

		protected override void OnException(ExceptionContext filterContext)
		{
			if (filterContext != null)
			{
				filterContext.ExceptionHandled = true;
				AddException($"{filterContext.Exception.Message}");
			}
			base.OnException(filterContext);
		}

		// Pode ser usado com JavaScript pra mostrar um toast na tela
		public void AddException(string message) =>
			TempData["ERROR"] = message;
	}
}