﻿using ProvaCandidato.Data.Entidade;
using System;
using System.Data.Entity;

namespace ProvaCandidato.Data
{
	public class ContextoPrincipal : DbContext
	{
		private const string DB_PATH = @"C:\Users\brmarko3\Desktop\provacandidato.mdf";

		private const string CONNECTION_STRING =
			@"Data Source=(LocalDB)\MSSQLLocalDB;" +
			@"AttachDbFilename=" + DB_PATH + ";" +
			@"Integrated Security=True;" +
			@"Connect Timeout=30";
		public ContextoPrincipal() : base(CONNECTION_STRING) { }

		public DbSet<Cliente> Clientes { get; set; }
		public DbSet<Cidade> Cidades { get; set; }
	}
}
