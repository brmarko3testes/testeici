﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProvaCandidato.Data.Entidade
{
	[Table("Cliente")]
	public class Cliente
	{
		private Cliente() { /* private empty constructor */ }

		public Cliente(DateTime? dataNascimento)
		{
			DataCriacao = DateTime.Now;

			#region Validação de DataNascimento:
			if (dataNascimento != null && dataNascimento < DataCriacao)
				DataNascimento = dataNascimento;
			else
			{
				// Geralmente aqui teríamos um ValidationResult.
				// No meu entender, validações de baixo nível DEVEM ser feitas server-
				// side enquanto as de alto nível PODEM ser feitas client-side.
				// Validação de baixo nível: Regras de Negócio.
				// Validação de alto nível: Formatação;
				DataNascimento = null;
			}
			#endregion
		}

		public DateTime DataCriacao { get; set; }

		[Key]
		[Column("codigo")]
		public int Codigo { get; set; }

		[Required]
		[Column("nome")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Deve conter entre 3 e 50 caracteres.")]
		public string Nome { get; set; }

		[Column("data_nascimento")]
		[DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
		public DateTime? DataNascimento { get; set; }

		[Column("codigo_cidade")]
		[Display(Name = "Cidade")]
		public int CidadeId { get; set; }

		public bool Ativo { get; set; }

		[ForeignKey("CidadeId")]
		public virtual Cidade Cidade { get; set; }
	}
}