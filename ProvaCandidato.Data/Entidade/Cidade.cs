﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProvaCandidato.Data.Entidade
{
	[Table("Cidade")]
	public class Cidade
	{
		public Cidade() =>
			DataCriacao = DateTime.Now;

		public DateTime DataCriacao { get; set; }

		[Key]
		[Column("codigo")]
		public int Codigo { get; set; }

		[Column("nome")]
		[StringLength(50, MinimumLength = 3)]
		[Required]
		public string Nome { get; set; }
	}
}
